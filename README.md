md2pdf
---

Bash script to compile a markdown file into pdf.
Just run `md2pdf file.md` and you'll get the pdf version of the markdown
file in the current folder.

Default font is Libertinus, which is available by default with a full
texlive installation.

The pages are numbered, it is possible to set a title, author and date with
yaml metadata:

```
---
title:
author:
date:
---
```

Tex Math is recognized with the dollar sign.
The font size is set to 12 pt.
The margins are set to 1.5 cm and the papersize is a4.
The default language is French.
